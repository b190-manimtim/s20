function determineUser(userNumber) {
    let username;

    switch (userNumber) {
        case 1:
            username = "unknownymous";
            break;
        case 2:
            username = "xXsteveJobsXx";
            break;
        case 3:
            username = "Doom";
            break;
        case 4:
            username = "Mryoso";
            break;
        case 5:
            username = "SidMeier";
            break
        default:
            username = userNumber + " not registered";
            break;
    }
    return username;
}

console.log(determineUser(6));