let myNumber = Number(prompt("Give me a number"));
console.log("The number you provided is " + myNumber);
for (let i = myNumber; i >= 0; i--) {

    if (i <= 50) {
        console.log("The current value is at " + i + ". Terminating the loop.")
        break;
    };

    if (i % 10 === 0) {

        console.log("The number is divisible by 10. Skipping the number.");
        continue;

    }

    if (i % 5 === 0) {
        console.log(i);

    }





};

let myString = "supercalifragilisticexpialidocious";

// const noVowels = myString.replace(/[aeiou]/gi, '');
// console.log(myString);
// console.log(noVowels);

console.log(myString);

function remVowel(str) {
    let noVowels = ['a', 'e', 'i', 'o', 'u',
        'A', 'E', 'I', 'O', 'U'
    ];
    let result = "";

    for (let i = 0; i < str.length; i++) {

        if (!noVowels.includes(str[i])) {
            result += str[i];

        }
    }
    return result;
}

console.log(remVowel(myString));